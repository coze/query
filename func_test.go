package main

import (
	"testing"
)

func TestQuery(t *testing.T) {
	deleteFile()
	checkFile()
	openDb()
	defer db.Close()
	initDb()

	result20_10 := len(Query(20, 10))
	if result20_10 != 0 {
		t.Error("Querying between 20 to 10 seconds failed. ", result20_10)
	}

	result200_100 := len(Query(200, 100))
	if result200_100 != 2 {
		t.Error("Querying between 200 to 100 seconds failed. ", result200_100)
	}

	result1000_100 := len(Query(1000, 100))
	if result1000_100 != 15 {
		t.Error("Querying between 1000 to 100 seconds failed. ", result1000_100)
	}
}
