package main

import (
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"os"
	"time"
)

type Record struct {
	gorm.Model
	Created time.Time
}

type RecordQuery struct {
	FromAgo int `json:"from_ago"`
	ToAgo   int `json:"to_ago"`
}

var SEED_SIZE = 60
var db *gorm.DB
var dbFileName = "records.db"
var shouldSeed = false

func main() {
	checkFile()
	openDb()
	defer db.Close()
	initDb()

	var rq RecordQuery
	json.NewDecoder(os.Stdin).Decode(&rq)
	printFirst()
	printLast()
	fmt.Printf("Scanning from %v to %v seconds ago.\n", rq.FromAgo, rq.ToAgo)
	fmt.Printf("There are %v records in db.\n",recordCount())
	fmt.Printf("Found %v matching records.\n", len(Query(rq.FromAgo, rq.ToAgo)))
}

func Query(from_ago int, to_ago int) []Record {
	now := time.Now()
	from := now.Add(time.Duration(-1*from_ago) * time.Second)
	to := now.Add(time.Duration(-1*to_ago) * time.Second)
	records := []Record{}
	db.Where("Created BETWEEN ? AND ?", from, to).Find(&records)
	return records
}

func recordCount() int {
	var count int
	db.Table("records").Count(&count)
	return count
}

func printFirst() {
	var record Record
	db.First(&record)
	fmt.Printf("First entry created : %v\n", record.Created)
}

func printLast() {
	var record Record
	db.Last(&record)
	fmt.Printf("Last entry created : %v\n", record.Created)
}

func checkFile() {
	fmt.Println("Checking DB")
	if _, err := os.Stat(dbFileName); os.IsNotExist(err) {
		shouldSeed = true
	}
}

func deleteFile() {
	fmt.Println("Deleting DB")
	os.Remove(dbFileName)
}

func openDb() {
	fmt.Println("Opening DB")
	var err error
	db, err = gorm.Open("sqlite3", dbFileName)
	if err != nil {
		panic("failed to connect database")
	}
}

func initDb() {
	if shouldSeed {
		fmt.Println("Seeding DB")
		db.AutoMigrate(&Record{})
		seedDb(SEED_SIZE)
	}
}

func seedDb(size int) {
	now := time.Now()
	for i := 1; i <= size; i++ {
		db.Create(&Record{Created: now})
		now = now.Add(-1 * time.Minute)
	}
}
